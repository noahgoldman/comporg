title "derp"
;-------------------------------------
stacksg segment para stack 'Stack'

stacksg ends
;-------------------------------------
datasg segment para 'Data'

    char_str db "AV$3zt*"
    num dw 34
    ans dw 0

datasg ends
;-------------------------------------
codesg segment para 'Code'
main proc far
    assume ss:stacksg, ds:datasg, cs:codesg
    mov ax, datasg
    mov ds, ax
    mov es, ax

    ; Question 1
    lea bx, char_str

    loopy:
        mov al, [bx]
        inc bx
        cmp al, 42
        je part2

        ; Compare the ascii values to find if the character is uppercase or lowercase
        cmp al, 65
        jl loopy
        cmp al, 90
        jle upper

        cmp al, 122
        jg loopy
        cmp al, 97
        jge lower

        jmp loopy

        ; Change an uppercase to a lowercase letter
        upper:
            add al, 32
            mov [bx-1], al
            jmp loopy

        ; Change a lowercase to an uppercase letter
        lower:
            sub al, 32
            mov [bx-1], al
            jmp loopy


    ; Question 2
    part2: mov ax, 1
    mov dx, num

    ; Loop through numbers starting with 2, if the integer squared is bigger
    ; then the number to find the square of then call closest
    loopx:
        inc ax 
        mov bx, ax 
        call mult

        cmp cx, dx
        jl loopx

        call closest

        mov ans, cx

    mov ax, 4c00h
    int 21h

main endp

mult proc near
    push ax
    push bx

    ; Take the absolute value of both numbers to perform multiplication
    call absolute

    mov cx, ax
    mov ax, 0
    addloop: add ax, bx
    loop addloop
    mov cx, ax

    pop bx
    pop ax 
    push ax
    
    ; Take the xor of the two numbers to find if there is a difference in the sign bit
    ;   If the sign bit is the same in both numbers, then the output is positive
    ;   If the sign bit is not the same, then the output is negative
    xor ax, bx
    ; Test the output of xor against a number with only a 1 in the sign bit
    test ax, 8000H

    ; If the zero flag is not set, then negate the output.  Otherwise, do nothing
    jnz negate 
    jmp endy

    negate:
        neg cx

    endy:
        pop ax
        ret
mult endp

; Take the absolute value of the numbers in ax and bx
absolute proc near
    cmp ax, 0
    jl abs_ax
    jmp second

    abs_ax: neg ax

    second: cmp bx, 0
        jge endr
        neg bx

    endr: ret
absolute endp

closest proc near
    push ax
    push bx 

    ; Find (ax)^2 and (ax-1)^2
    push dx
    mov bx, ax
    call mult
    mov dx, cx
    sub ax, 1
    sub bx, 1
    call mult
    mov bx, dx
    pop dx

    ; Find the difference between the squares and the actual number.
    sub bx, dx
    sub cx, dx
    
    mov ax, cx

    ; Take the absolute values of both of the differences
    call absolute

    ; Compare the differences and output the corresponding value
    cmp bx, ax

    pop bx
    pop ax

    jg retc

    mov cx, ax
    jmp endx

    retc:
        sub ax, 1
        mov cx, ax
        add ax, 1
        
    endx: ret
closest endp


codesg ends

end main
