# Program documentation goes here.

.data
    x: .word 10, 20, 34, 24, 12, 36, 9, 7
    z: .half 1, -2, 3, 1, -3, 6, -7, 3, 0
    newline: .asciiz "\n"
           

.text                           
.globl main			 # Main (must be global).
	
main: 

    la $t0, x
    li $t2, 7
    lw $s0, 0($t0)
    lw $s1, 0($t0)

    loopr:
        addi $t0, $t0, 4 
        lw $t1, 0($t0)

        ble $t1, $s0, check_min
            add $s0, $t1, $zero 

        check_min:
        bge $t1, $s1, end_loop
            add $s1, $t1, $zero

        end_loop: 
            addi, $t2, $s2, -1
            bgtz $t2, loopr


    # Part b
    la $t0, x
    li $t2, 8
    li $s2, 0

    li $t5, 4

    loopy:
        lw $t1, 0($t0)
        div $t1, $t5
        mfhi $t4

        bne $t4, $zero, endl
        addi $s2, $s2, 1

        endl:
            addi $t2, $t2, -1
            addi $t0, $t0, 4
            bgtz $t2, loopy


    add $a0, $s2, $zero
    li $v0, 1
    syscall

    # Part c
    la $t0, z
    li $s3, 0

    loopz:
        lh $t1, 0($t0)
        beqz $t1, endp

        # Absolute value
        bgtz $t1, add_next
        sub $t1, $zero, $t1
        
        add_next:
        add $s3, $s3, $t1
        addi $t0, $t0, 2
        j loopz

    endp:

    # Print a newline character
    la $a0, newline
    li $v0, 4
    syscall


    add $a0, $s3, $zero
    li $v0, 1
    syscall
		
#CODE HERE
	li $v0, 10                    # Syscall to exit.
	syscall
