title "HW5 - Noah Goldman"
;-------------------------------------
stacksg segment para stack 'Stack'
    db 32 dup(0)
stacksg ends
;-------------------------------------
datasg segment para 'Data'
    first_line db "Enter the first number: $$$$"    
    second_line db "Enter the second number: $"
    third_line db "Enter the operation: $"
    last_line db 14 dup('$')
    output db 7 dup('$')
    remainder_out db " Remainder: $"
    remainder db 5 dup('$')

    int_list1 label byte
        int_maxlen1 db 5
        int_actlen1 db ?
        int_data1 db 6 dup('$')

    int_list2 label byte
        int_maxlen2 db 5
        int_actlen2 db ?
        int_data2 db 6 dup('$')

    op_list label byte
        op_maxlen db 2
        op_actlen db ?
        op_data db 2 dup('$')
        
datasg ends
;-------------------------------------
codesg segment para 'Code'
main proc far
    assume ss:stacksg, ds:datasg, cs:codesg
    
    mov ax, datasg
    mov ds, ax
    mov es, ax

    ; Setting background color to cyan and yellow
    mov ah, 06H
    mov cx, 0000H
    mov dx, 184fH
    mov al, 0
    mov bh, 36H
    int 10H
    ; Move the cursor
    mov ah, 02H
    mov dh, 8
    mov dl, 10
    mov bh, 0
    int 10H

    ; Print the initial text
    mov ah, 09H
    lea dx, first_line
    int 21H

    ; Move the cursor down for the second line
    mov ah, 02H
    mov dh, 10 
    mov dl, 10
    mov bh, 0
    int 10H

    ; Print the 
    mov ah, 09H
    lea dx, second_line
    int 21H

    ; Move the cursor down for the third line
    mov ah, 02H
    mov dh, 12 
    mov dl, 10
    mov bh, 0
    int 10H

    ; Print the 
    mov ah, 09H
    lea dx, third_line
    int 21H

    ;--------------------------------------------------------
    ;---------------------- Accept Inputs -------------------
    ;--------------------------------------------------------

    start:

    ; Clear the old stuff

    mov ah, 06H
    mov cx, 0822H
    mov dx, 0830H
    mov al, 0
    mov bh, 36H
    int 10H

    mov ah, 06H
    mov cx, 0A23H
    mov dx, 0A30H
    mov al, 0
    mov bh, 36H
    int 10H

    mov ah, 06H
    mov cx, 0C1EH
    mov dx, 0C30H
    mov al, 0
    mov bh, 36H
    int 10H

    mov ah, 02H
    mov dh, 8
    mov dl, 34
    mov bh, 0
    int 10H

    ; First line
    mov ah, 0AH
    lea dx, int_list1
    int 21H

    lea bx, int_data1
    cmp byte ptr[bx], '0'
    je jmp_endl

    jmp nextr
    jmp_endl:
        jmp endl
    nextr:

    mov ah, 02H
    mov dh, 10 
    mov dl, 35
    mov bh, 0
    int 10H

    mov ah, 0AH
    lea dx, int_list2
    int 21H

    mov ah, 02H
    mov dh, 12 
    mov dl, 31
    mov bh, 0
    int 10H

    mov ah, 0AH
    lea dx, op_list
    int 21H

    ;--------------------------------------------------------
    ;---------------------- Processing ----------------------
    ;--------------------------------------------------------

    lea bx, int_data1
    call str_to_int 
    mov ax, dx

    lea bx, int_data2
    call str_to_int
    mov bx, dx

    call make_last_line

    call perform_operation

    mov dx, 0
    lea bx, op_data
    cmp byte ptr[bx], '/'
    je is_div
        mov dx, cx
        jmp nextl
    is_div:
        mov al, cl
        cbw
        mov dx, ax

    nextl:
    lea di, output
    call int_to_str

    mov dx, 0
    mov al, ch
    cbw
    mov dx, ax
    lea di, remainder
    call int_to_str


    ;--------------------------------------------------------
    ;---------------------- Final Output --------------------
    ;--------------------------------------------------------

    mov ah, 06H
    mov cx, 0E00H
    mov dx, 0E40H
    mov al, 0
    mov bh, 36H
    int 10H

    ; Move the cursor down for the third line
    mov ah, 02H
    mov dh, 14 
    mov dl, 10
    mov bh, 0
    int 10H

    ; Print the 
    mov ah, 09H
    lea dx, last_line
    int 21H

    mov ah, 09H
    lea dx, output
    int 21H

    lea bx, op_data
    cmp byte ptr[bx], '/'
    jne jmp_start

    mov ah, 09H
    lea dx, remainder_out
    int 21H
    
    mov ah, 09H
    lea dx, remainder
    int 21H

    jmp far ptr start

    endl:
    mov ax, 4c00h
    int 21h

    jmp_start:
        jmp start
main endp

; Takes bx as an input with the address of the string
; Outputs dx as the int value
str_to_int proc near 
    push ax
    push cx
    push bx 

    mov ax, 0
    mov cx, 0
    mov dx, 0
    
    mov cl, [bx]   

    ; Check if the first char is a negative sign
    cmp cx, '-'
    jne deep
    inc bx

    deep:
        mov cl, [bx]
        sub cl, 30H
        add dl, cl

        inc bx
        cmp byte ptr[bx], 0DH
        je check_neg

        mov al, 10   
        mul dl
        mov dl, al
        jmp deep

    ; Check to see if the number should be negated, and do so if possible
    check_neg:
        pop bx  
        mov cl, [bx]
        cmp cl, '-' 
        jne endr
        neg dx

    endr:
        pop cx
        pop ax
        ret
                
str_to_int endp

int_to_str proc near
    push ax
    push bx
    push cx
    push dx 
    push si

    mov ax, 0
    mov ax, dx

    mov ax, dx
    mov si, dx
    mov dx, 0
    mov cx, 0

    cmp ax, 0
    jge excrete
    neg ax

    excrete:
        mov bx, 10
        div bx    

        mov bx, 0
        mov bl, dl
        push bx
        inc cx
        mov dx, 0

        cmp ax, 0
        jne excrete

        ; Check if the original number was negative
        cmp si, 0
        jge pvals

    add_neg:
        mov byte ptr[di], '-'
        inc di

    pvals:
        pop ax 
        add al, 30H
        mov [di], al
        inc di
        loop pvals

    endi:
        mov byte ptr[di], '$'
        pop si
        pop dx
        pop cx
        pop bx
        pop ax
        ret
int_to_str endp

perform_operation proc near
    push ax
    push bx

    mov cl, op_data

    ; Addition
    cmp cl, '+'
    jne subtract

        add ax, bx
        mov cx, ax
        jmp endo

    subtract:
        cmp cl, '-'
        jne multiply

        sub ax, bx
        mov cx, ax
        jmp endo

    multiply:
        cmp cl, '*'
        jne divide

        imul bl
        mov cx, ax
        jmp endo

    divide:
        cwd
        cmp cl, '/'
        jne endo
        
        idiv bl
        mov cx, ax

    endo:
        pop bx
        pop ax
        ret
perform_operation endp

make_last_line proc near
    push ax
    push bx
    push cx
    push dx
    push si
    
    lea si, last_line
    lea bx, int_data1

    int1:
        mov al, [bx]
        inc bx
        cmp al, 0DH
        je next1
 
        mov [si], al
        inc si
        jmp int1

    next1:
        mov byte ptr[si], ' '
        inc si
        lea bx, op_data
        mov al, [bx]
        mov [si], al
        inc si
        mov byte ptr[si], ' '
        inc si

    lea bx, int_data2
    
    int2:
        mov al, [bx]
        inc bx
        cmp al, 0DH
        je next2

        mov [si], al
        inc si
        jmp int2

    next2:
        mov byte ptr[si], ' '
        inc si
        mov byte ptr[si], '='
        inc si
        mov byte ptr[si], ' '
        inc si
        mov byte ptr[si], '$'
        
    pop si
    pop dx
    pop cx
    pop bx
    pop ax
    ret
make_last_line endp

codesg ends

end main
